import { Component, OnInit } from '@angular/core';
import { ProductsService } from './products.service';

@Component({
  selector: 'jce-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
isLoading:Boolean = true;
  products;
  constructor(private _productsService:ProductsService) { }
  deleteProduct(product){
    this._productsService.deleteproduct(product);//add user through service file where there is an add user function.       
  }
  ngOnInit() {
       this._productsService.getProducts().subscribe(postsData => 
    {this.products = postsData;
       this.isLoading =false})

  }
  addproduct(product){
      this._productsService.addproduct(product);
  }

}
