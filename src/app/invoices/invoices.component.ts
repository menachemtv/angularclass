import { Component, OnInit } from '@angular/core';
import {InvoicesService} from './invoices.service';

@Component({
  selector: 'jce-invoices',
  templateUrl: './invoices.component.html',
    styles: [`
     .users li { cursor: default; }
     .users li:hover { background: #ecf0f1; } 
     .list-group-item.active, 
     .list-group-item.active:hover { 
          background-color: #ecf0f1;
          border-color: #ecf0f1; 
          color: #2c3e50;
     }
   `]
})
export class InvoicesComponent implements OnInit {
invoices1;
 isLoading:Boolean = true;
      invoices= [
      {product:'sedfsdf',amount:'2323'},
     {product:'vewv',amount:'32'},
      {product:'dsgr',amount:'45435'}
    ]

  constructor(private _invoicesService:InvoicesService) { }
  addinvoice(invoice){
    this.invoices1.push(invoice),1//push is a function on array, adds object in end of array.
  
}
  ngOnInit() {
 this._invoicesService.getUsers().subscribe(usersData =>
    {this.invoices1 = usersData;
    this.isLoading = false})

  }

}
