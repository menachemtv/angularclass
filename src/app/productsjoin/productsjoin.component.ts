import { Component, OnInit } from '@angular/core';
import { ProductsjoinService } from './productsjoin.service';
@Component({
  selector: 'jce-productsjoin',
  templateUrl: './productsjoin.component.html',
  styleUrls: ['./productsjoin.component.css']
})
export class ProductsjoinComponent implements OnInit {
isLoading:Boolean = true;
  products;
  constructor(private _productsjoinService:ProductsjoinService) { }
  deleteProduct(product){
    this._productsjoinService.deleteproduct(product);//add user through service file where there is an add user function.       
  }
  ngOnInit() {
       this._productsjoinService.getProducts().subscribe(postsData => 
    {this.products = postsData;
       this.isLoading =false})

  }

}