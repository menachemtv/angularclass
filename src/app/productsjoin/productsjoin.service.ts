import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {AngularFire} from 'angularfire2';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';//function that delays loading of data

@Injectable()
export class ProductsjoinService {
  productsObservable;
  getProducts (){
    this.productsObservable = this.af.database.list('/product/').map(
            products => {
        products.map(
          product=> {
            product.productCategory = [];

             product.productCategory.push(
                this.af.database.object('/category/' +product.categoryId)
              )
  
          }
        );
        return products;
      }
    );
    return this.productsObservable;
//return this._http.get(this._url).map(res => res.json()).delay(2000)

  }
  constructor(private af:AngularFire) { }
  deleteproduct(product){
let productKey = product.$key;
this.af.database.object('/product/' + productKey).remove();//product is name of list in angular
    }
}
