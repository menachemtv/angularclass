import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Product1} from './product1';

@Component({
  selector: 'jce-product1',
  templateUrl: './product1.component.html',
  styleUrls: ['./product1.component.css'],
    inputs:['product1']
})
export class Product1Component implements OnInit {
  product1:Product1;//create varibale variable user that is class user 
  @Output() deleteEvent = new EventEmitter<Product1>()
  constructor() { }
  sendDelete(){
    this.deleteEvent.emit(this.product1);
  }
  ngOnInit() {
  }

}
