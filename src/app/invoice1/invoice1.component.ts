import { Component, OnInit } from '@angular/core';
import {Invoice1} from './invoice1';
@Component({
  selector: 'jce-invoice1',
  templateUrl: './invoice1.component.html',
  styleUrls: ['./invoice1.component.css'],
  inputs:['invoice1']
})
export class Invoice1Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
