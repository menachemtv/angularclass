import { Injectable } from '@angular/core';
import {AngularFire} from 'angularfire2';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';//function that delays loading of data

@Injectable()
export class InvoicesService {
  invoicesObservable;
  getUsers(){
    this.invoicesObservable= this.af.database.list('/invoices');//product is name of list in angular
    return this.invoicesObservable;

  }
  constructor(private af:AngularFire) { }

}
