import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {RouterModule, Routes} from '@angular/router';
import { AngularFireModule } from 'angularfire2';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { UsersService } from './users/users.service';
import { InvoicesService } from './invoices/invoices.service';
import { ProductsService } from './products/products.service';
import { ProductsjoinService } from './productsjoin/productsjoin.service';
import { UserComponent } from './user/user.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PostsComponent } from './posts/posts.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserFormComponent } from './user-form/user-form.component';
import { ProductComponent } from './product/product.component';
import { ProductsComponent } from './products/products.component';
import { Product1Component } from './product1/product1.component';
import { ProductFormComponent } from './product-form/product-form.component';
import { ProductsjoinComponent } from './productsjoin/productsjoin.component';
import { InvoiceFormComponentComponent } from './invoice-form-component/invoice-form-component.component';
import { InvoicesComponent } from './invoices/invoices.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { Invoice1Component } from './invoice1/invoice1.component';

 export const firebaseConfig = {
    apiKey: "AIzaSyCltZoazlcfTyEe4tVSRk0r8FSINAiyPkk",
    authDomain: "angularclass-346d3.firebaseapp.com",
    databaseURL: "https://angularclass-346d3.firebaseio.com",
    storageBucket: "angularclass-346d3.appspot.com",
    messagingSenderId: "849525226262"
 }
const appRoutes:Routes = [ //building routes according to url entered.

  {path:'invoice-form-component',component:InvoiceFormComponentComponent},//when relize path is posts load post component.
  {path:'products',component:ProductsComponent},//when relize path is products load product component.
  {path:'invoices',component:InvoicesComponent},//when relize path is products load product component.
  {path:'',component:InvoicesComponent},// when no path inserted load user component as default. 
  {path:'**',component:PageNotFoundComponent}//when path doesn't exist, load error component.
]

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserComponent,
    SpinnerComponent,
    PostsComponent,
    PageNotFoundComponent,
    UserFormComponent,
    ProductComponent,
    ProductsComponent,
    Product1Component,
    ProductFormComponent,
    ProductsjoinComponent,
    InvoiceFormComponentComponent,
    InvoicesComponent,
    InvoiceComponent,
    Invoice1Component,

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [UsersService, ProductsService,ProductsjoinService, InvoicesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
