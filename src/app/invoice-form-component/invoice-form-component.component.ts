import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Invoice1} from '../invoice1/invoice1'; //import user component
import {NgForm} from '@angular/forms';


@Component({
  selector: 'jce-invoice-form-component',
  templateUrl: './invoice-form-component.component.html',
  styleUrls: ['./invoice-form-component.component.css']
})
export class InvoiceFormComponentComponent implements OnInit {
@Output() invoiceAddedEvent= new EventEmitter<Invoice1>();

invoice:Invoice1 = {product:'', amount:''};

onSubmit(form:NgForm){
console.log(form);
this.invoiceAddedEvent.emit(this.invoice);
this.invoice= {
  product:'',
  amount:''
}
}
  constructor() { }

  ngOnInit() {
  }

}
