/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ProductsjoinService } from './productsjoin.service';

describe('Service: Productsjoin', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProductsjoinService]
    });
  });

  it('should ...', inject([ProductsjoinService], (service: ProductsjoinService) => {
    expect(service).toBeTruthy();
  }));
});
